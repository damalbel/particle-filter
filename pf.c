#include "pf.h"


void initialize_cloud(struct p_cloud *pcloud, int n, int sydim){

    int i;

    pcloud->n = n;
    pcloud->dim = sydim;
    pcloud->particle = calloc(n, sizeof(particle));

    for(i = 0; i < n; i++){
        pcloud->particle[i].i = i;
        pcloud->particle[i].w = 1.0/n;
    }

}

void normalize_cloud(struct p_cloud *pcloud){

    int i, n = pcloud->n;
    double cweight = 0.0;

    for (i = 0; i < n; i++)
        cweight += pcloud->particle[i].w;

    for (i = 0; i < n; i++)
        pcloud->particle[i].w = pcloud->particle[i].w/cweight;
}


void free_cloud(struct p_cloud *pcloud){
    free(pcloud->particle);
}

void print_particle_data(struct p_cloud *pcloud){

    int i;

    for (i = 0; i < pcloud->n; i++){
        printf("Particle # %d. weight = %g\n", pcloud->particle[i].i,
                pcloud->particle[i].w);
        printf("\tx[0] = %g\n", pcloud->particle[i].x[0]);
    }

}

void resample(struct p_cloud *pcloud, gsl_rng * r){

    /* Implements multinomial resampling */


    int i, j;
    double *c = calloc(pcloud->n, sizeof(double));
    double u0, u, N1 = (1.0/pcloud->n);

    /* Create cloud structure */
    struct p_cloud res_cloud; /* resampled cloud */
    initialize_cloud(&res_cloud, pcloud->n, pcloud->dim);


    /* Cumulative weight vector */
    c[0] = pcloud->particle[0].w;
    for (i = 1; i < pcloud->n; i++)
        c[i] = c[i - 1] + pcloud->particle[i].w;

    u0 = gsl_ran_flat(r, 0, N1);

    for (j = 0; j < pcloud->n; j++){
        u = u0 + N1*j;
        i = 0;
        while (u > c[i])
            i += 1;

        /* assign particle */
        memcpy(res_cloud.particle[i].x, pcloud->particle[j].x,
            MAX_STATE_SIZE * sizeof(double));
        res_cloud.particle[j].w = pcloud->particle[i].w;

    }

    /* Copying values back to original cloud.
     * NOTE: I tried to substitute the pointer to pcloud to 
     * res_cloud but I got a weird error. I think it should be 
     * more efficient. I will have to come back to this.
     */
    

    for (i = 0; i < pcloud->n; i++){
        pcloud->particle[i].x[0] = res_cloud.particle[i].x[0];
        memcpy(pcloud->particle[i].x, res_cloud.particle[i].x,
            MAX_STATE_SIZE * sizeof(double));
        pcloud->particle[i].w = N1;
    }

    free_cloud(&res_cloud); 
    free(c);
}


void resample_systematic(struct p_cloud *pcloud, gsl_rng * r){

    /* Implements multinomial resampling */


    int i, j;
    double s = 0.0;

    int n = pcloud->n;
    
    /* Create cloud structure */
    struct p_cloud res_cloud; /* resampled cloud */
    initialize_cloud(&res_cloud, pcloud->n, pcloud->dim);


    double u = (double)gsl_rng_uniform(r)/n;

    j = 0;

    for (i = 0; i < n; i++) {

        s += pcloud->particle[i].w;

        while (u <= s && j < n) {

            /* assign particle */
            memcpy(res_cloud.particle[j].x, pcloud->particle[i].x,
                MAX_STATE_SIZE * sizeof(double));
            j++;
            u += (double)1.0/(n);

        }

        if (j == n)
            break;

    }

    /* Copying values back to original cloud.
     * NOTE: I tried to substitute the pointer to pcloud to 
     * res_cloud but I got a weird error. I think it should be 
     * more efficient. I will have to come back to this.
     */
    

    for (i = 0; i < pcloud->n; i++){
        pcloud->particle[i].x[0] = res_cloud.particle[i].x[0];
        memcpy(pcloud->particle[i].x, res_cloud.particle[i].x,
            MAX_STATE_SIZE * sizeof(double));
        pcloud->particle[i].w = 1.0/n;
    }

    free_cloud(&res_cloud); 
}


void calc_neff(struct p_cloud pcloud, double *neff) {

    int i;

    *neff = 0.0;

    for (i = 0; i < pcloud.n; i++)
        *neff += pow(pcloud.particle[i].w, 2.0);

    *neff = 1.0/(*neff);

}
