import subprocess
import numpy as np
import h5py
import os

#change to script directory
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


#execute program
args = ['./test1']

print "Running test #1: cappe example"

subprocess.call(args)

h5file = h5py.File('xmean.h5','r')
xmean  = np.array(h5file.get('xmean'))
xtoy   = np.loadtxt('xtoy.txt', delimiter='\n')

error_mine = np.sum(np.abs(xtoy - xmean))

if error_mine > 280.0:
    print "TEST #1 ERROR: 1-norm is: " + str(error_mine)
else:
    print "TEST #1 PASSED"


