#include "../../pf.h"

/* These will be the user-defined functions.
 * Might have to provide some kind of generic interface?
 */


void likelihood(double *w, double *x, gsl_rng *r, double meas) {
    
    double sigma_v = 1.0;
    double y;

    y = pow(x[0], 2.0)/20.0 + gsl_ran_gaussian(r, sigma_v);
    w[0] *= gsl_ran_gaussian_pdf(meas - y, sigma_v);
    //w[0] *= -0.5*log(2*M_PI)-0.5*pow(meas-(double)(pow(x[0],2)/20.0),2);
}

void propagate(double *x, gsl_rng *r, double t) {
    /* Bogus propagation */

    double sigma_w = 3.16227766016838;
    x[0] = x[0]/2.0 + 25.0*(x[0]/(1 + pow(x[0], 2.0))) +
        8.0*cos(1.2*t) + gsl_ran_gaussian(r, sigma_w);


}

void prior(double *x, gsl_rng *r) {
    
    double sigma_w = 10.0;
    
    x[0] = gsl_ran_gaussian(r, sigma_w);
}

int main (int argc, char **argv)
{

    hid_t   file_id;
    hsize_t dims[2];
    int     i, j, npart, ncycl, sydim;
    clock_t start, end;
    long    seed;

    double  *meas, neff, elapsed_time, *xmean;

    /* problem paramenters */
    npart = 128;
    ncycl = 100; /*number of cycles*/
    sydim = 1;   /* dimension of system */

    /* read measurement data */
    file_id = H5Fopen ("y.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
    H5LTget_dataset_info(file_id,"measurements",dims,NULL,NULL);
    meas    = malloc(dims[0]*sizeof(double));
    H5LTread_dataset_double(file_id,"measurements",meas);
    H5Fclose (file_id);

    
    /* GSL random stuff */
    const gsl_rng_type * T;
    gsl_rng * r;

    gsl_rng_env_setup();
    T = gsl_rng_default;
    r = gsl_rng_alloc (T);
    seed = time (NULL) * getpid();
    gsl_rng_set (r, seed);

    /* Create particles and other structures */
    assert(sydim < MAX_STATE_SIZE);
    assert(ncycl == dims[0]);

    struct p_cloud p_cloud;
    initialize_cloud(&p_cloud, npart, sydim);

    xmean = calloc(ncycl, sizeof(double));

    /* Sample from prior and perform first step*/
    for (j = 0; j < npart; j++)
        prior(p_cloud.particle[j].x, r);
        likelihood(&p_cloud.particle[j].w, p_cloud.particle[j].x, 
                    r, meas[0]);

    normalize_cloud(&p_cloud);

    for (j = 0; j < npart; j++)
        xmean[0] += p_cloud.particle[j].w*p_cloud.particle[j].x[0];
    
#ifdef DEBUG
    print_particle_data(&p_cloud);
#endif
    
    /* TIME LOOP */

    for (i = 1; i < ncycl; i++ ) {
        start = clock() ;

        /* resampling */
        calc_neff(p_cloud, &neff);
        resample_systematic(&p_cloud, r);

        /* propagate and calc. likelihood */
        for (j = 0; j < npart; j++) {
            propagate(p_cloud.particle[j].x, r, (double)(i));
            likelihood(&p_cloud.particle[j].w, p_cloud.particle[j].x, 
                    r, meas[i]);
        }
        normalize_cloud(&p_cloud);
        
#ifdef DEBUG
        printf("\nMeasurement: %g.\n", meas[i]);
        print_particle_data(&p_cloud);       
#endif

        for (j = 0; j < npart; j++)
            xmean[i] += p_cloud.particle[j].w*p_cloud.particle[j].x[0];
        
        end = clock() ;
        elapsed_time = (end-start)/(double)CLOCKS_PER_SEC;
        printf("Step: %d. Time cost: %5.5f ms. Neff: %5.5f. xm = %g\n", i, elapsed_time*1000.0, neff, xmean[i]);
    }

    /* save mean vector */

    dims[0] = ncycl;
    file_id = H5Fcreate ("xmean.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    H5LTmake_dataset(file_id,"xmean",1,dims,H5T_NATIVE_DOUBLE,xmean);
    H5Fclose (file_id);



    /* free variables */
    free_cloud(&p_cloud);
    free(meas);
    gsl_rng_free (r);


    return 0;
}
