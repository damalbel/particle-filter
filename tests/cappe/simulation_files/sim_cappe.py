# Simulate unidimensional example found in cappe's paper

import numpy as np
from scipy.stats import norm
import h5py

# simulation parameters
sigma_w = 1.0
sigma_v = 1.0

# simulation parameters
nsteps = 100

# model parameters
sigma_w = 10.0
sigma_v = 1.0

# simulation variables

x_true = np.zeros(nsteps)
y      = np.zeros(nsteps)
t      = np.linspace(0, 100, nsteps)

def prior():
    return np.random.normal(0.0, 10, 1)

def propagate(x, t):
    return x/2.0 + 25.0*(x/(1 + x**2.0)) + 8.0*np.cos(1.2*t) + np.random.normal(0.0, sigma_w, 1)

def measure(x):
    return (x**2.0)/20.0 + np.random.normal(0.0, sigma_v, 1)

x0 = prior()
x0 = 0.0

for i in range(nsteps):
    if i > 0:
        x_true[i] = propagate(x_true[i - 1], t[i])
        y[i]      = measure(x_true[i])
    else:
        x_true[i] = x0
        y[i]      = measure(x0)

# save results to file

y_out = h5py.File("y.h5", "w")
y_out.create_dataset('measurements', data=y)
y_out.close()

x_out = h5py.File("xtrue.h5", "w")
x_out.create_dataset('xtrue', data=x_true)
x_out.close()
#x_true.tofile('xtrue.txt', sep=",", format="%s")
#y.tofile('y.txt', sep=",", format="%s")
        

