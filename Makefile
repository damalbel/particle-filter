CC=h5cc
CFLAGS=-O0 -Wall -g -I/usr/include/gsl
LFLAGS=-lgsl -lgslcblas -lm

all: cappe

debug: CFLAGS += -DDEBUG
debug: all

# cappe test

run_cappe:
	python tests/cappe/test1_script.py

cappe: test1.o pf.o
	$(CC) -o  tests/cappe/test1 test1.o pf.o $(LFLAGS)

test1.o: tests/cappe/test1.c pf.h
	$(CC) $(CFLAGS) -c tests/cappe/test1.c

# particle filter routines
pf.o: pf.c pf.h
	$(CC) $(CFLAGS) -c pf.c

clean::
	$(RM) *.o
