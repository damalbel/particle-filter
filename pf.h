#ifndef PF_H
#define PF_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include "hdf5.h"
#include "hdf5_hl.h"

#define MAX_STATE_SIZE 10

typedef struct particle {
    double  x[MAX_STATE_SIZE];
    double  w;
    int     i;
}particle;

struct p_cloud {
    int n; /* number of particles */
    int dim; /* dimension of state vector */
    struct particle *particle;
};


/* Function declarations */

void initialize_cloud(struct p_cloud *pcloud, int n, int sydim);

void normalize_cloud(struct p_cloud *pcloud);

void free_cloud(struct p_cloud *pcloud);

void print_particle_data(struct p_cloud *pcloud);

void resample(struct p_cloud *pcloud, gsl_rng * r);

void resample_systematic(struct p_cloud *pcloud, gsl_rng * r);

void calc_neff(struct p_cloud pcloud, double *neff);

#endif
