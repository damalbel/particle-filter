# Particle Filter #

A high performance particle filtering library in C.

TODO: reimplement particle cloud, instead of array of particle structs, each state is an array. Avoid cache miss. Use gsl for means, variances, etc...


### Useful links ###

* [Bayesian Filtering: From Kalman Filters to Particle Filters, and Beyond](http://www.dsi.unifi.it/users/chisci/idfric/Nonlinear_filtering_Chen.pdf)
* [An introduction to particle filters](http://dip.sun.ac.za/~herbst/MachineLearning/ExtraNotes/ParticleFilters.pdf)
